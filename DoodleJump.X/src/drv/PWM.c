/**
 * \file 		PWM.h
 * \brief 		PWM driver header file
 * \authors 	Pierre Bergeras
 * \version 	1.0
 * \date 		15/11/2021 - 27/11/2021
 *
 * The PWM driver is responsible for controlling the 
 * the PWM embedded in the CCP1 module
 * 
 * It allows you to setup PWM module from CCP1
 * 
 * \pre       First initialize the driver with PWM_init() function
 */

#include "PWM.h"		/** Include LED driver functions 			*/

/**
 * @brief      Initializes the pwm driver
 * 
 * Configure CCP1 as a PWM (driven by timer 2)
 * 
 */
void PWM_Init(void)
{
	// CCP1M P1A,P1C: active high; P1B,P1D: active high; DC1B 3; P1M single; 
	CCP1CON = 0x3C;    
	
	// CCP1ASE operating; PSS1BD low; PSS1AC low; CCP1AS disabled; 
	ECCP1AS = 0x00;    
	
	// CCPR1H 0; 
	CCPR1H = 0x00;    
	
	// CCPR1L 77; 
	CCPR1L = 0x4D;    
}
