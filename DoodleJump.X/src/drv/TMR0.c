/**
 * \file 		tmr0-driver.h
 * \brief 		Timer 0 driver source file
 * \authors 	Gabin Blot
 * \version 	1.0
 * \date 		15/08/2020
 *
 * Source code of the timer 0 driver
 * 
 *  
 * \pre       First initialize the driver with INTERRUPT_Init() function
 */


/**
  Section: Included Files
*/

#include <xc.h>
#include "TMR0.h"

/**
  Section: Global Variables Definitions
*/

void (*TMR0_InterruptHandler)(void);

volatile unsigned short timer0ReloadVal;

/**
  Section: TMR0 APIs
*/


void TMR0_Init(void)
{
    T0CONbits.T08BIT = 0;   /** 16 bit mode for timer 0 */

    TMR0_Reload();          /** Initialize timer 0 value */
	
    INTCONbits.TMR0IF = 0;  /** Clear Interrupt flag before enabling the interrupt */

    INTCONbits.TMR0IE = 1;  /** Enabling TMR0 interrupt */

    T0CONbits.TMR0ON = 1;       /** Make sure Timer 0 is off before changing its settings */
    T0CONbits.T08BIT = 0;       /** Timer as a 16 bit counter */
    T0CONbits.T0CS   = 0;       /** Use internal clock */
    T0CONbits.T0SE   = 1;       /** Increment on rising edged (arbitrary lets see if it works like that) */
    T0CONbits.PSA    = 0;       /** Use the prescaler as timer input */
    T0CONbits.T0PS   = 0b111;   /** Prescaler configured at 1/16 (1MHz @ FOSC = 16 MHz) */
    
    
    //T0CON = 0x97;           
    /** T0PS 1:256; 
                             * T08BIT 16-bit; 
                             * T0SE Increment_hi_lo; 
                             * T0CS FOSC/4; 
                             * TMR0ON enabled; 
                             * PSA assigned 
                             */
}

void TMR0_StartTimer(void)
{
    T0CONbits.TMR0ON = 1; /** Start the Timer by writing to TMR0ON bit */
}

void TMR0_StopTimer(void)
{
    T0CONbits.TMR0ON = 0; /** Stop the Timer by writing to TMR0ON bit */
}

unsigned short TMR0_ReadTimer(void)
{
    return TMR0;
}

void TMR0_WriteTimer(unsigned short timerVal)
{
    TMR0 = timerVal;
}

void TMR0_Reload(void)
{
    TMR0 = _INIT_TMR0; /** Reload initial value into TMR0 */
}

void TMR0_ISR(void)
{
    INTCONbits.TMR0IF = 0;  /** Clear the TMR0 interrupt flag */
    TMR0_Reload();          /** Reload timer 0 init value */
}
