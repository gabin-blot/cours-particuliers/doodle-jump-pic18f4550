/**
 * \file 		EEPROM.h
 * \brief 		EEPROM driver header file
 * \authors 	Pierre Bergeras
 * \version 	1.0
 * \date 		15/11/2021 - 27/11/2021
 *
 * The EEPROM driver is responsible for controlling the 
 * the EEPROM embedded in the PIC18F4550
 * 
 * It allows you to read and write on EEPROM
 * 
 * \pre       None
 */

#pragma once

void EEPROM_write(unsigned char msg, unsigned char address);
char EEPROM_read(unsigned char address);
void EEPROM_writeString(char* msg, unsigned char address);
unsigned int EEPROM_readString(char * msg, unsigned int size, unsigned char address);
