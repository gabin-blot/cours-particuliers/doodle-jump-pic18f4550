/* 
 * File:   USART.h
 * Author: maxime
 *
 * Created on 27 novembre 2020, 14:42
 */

#ifndef USART_H
#define	USART_H


#include <xc.h>
#pragma config FOSC = HS
#pragma config WDT = OFF
//#define Fosc 500000 //Si servomoteur ?
#define Fosc 8000000
#define _XTAL_FREQ 8000000



extern void usartInit(int baudRate);
extern char usartRead();
extern char usartCheck();

#endif	/* USART_H */

