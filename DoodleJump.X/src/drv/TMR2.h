
#ifndef TMR2_DRIVER_H
#define TMR2_DRIVER_H

#include <stdint.h>
#include <stdbool.h>


/**
  Section: Macro Declarations
*/

#define _INIT_TMR2  0xC2F6
#define _MAX_TMR2   0xFFFF 
#define _MIN_TMR2   _INIT_TMR2 

/**
  Section: TMR2 APIs
*/

/**
 * @brief Initializes the TMR2
 *  This function initializes the TMR2 Registers.
 *  This function must be called before any other TMR2 function is called.
 */
void TMR2_Init(void);


/**
 * @brief This function starts the TMR2
 * 
 * This function starts the TMR2 operation
 * This function must be called after the initialization of TMR2
 * 
 * @pre Initialize the TMR2 before calling this function
 */
void TMR2_StartTimer(void);


/**
 * @brief This function stops the TMR2
 * 
 * This function stops the TMR2 operation
 * This function must be called after the start of TMR2
 * 
 * @pre Initialize the TMR2 before calling this function
 */
void TMR2_StopTimer(void);


/**
 * @brief Reads the 16 bits TMR2 register value
 * 
 * This function reads the 16 bits TMR2 register value and return it
 * 
 * @pre Initialize the TMR2 before calling this function
 */
unsigned short TMR2_ReadTimer(void);


/**
 * @brief Reads the 16 bits TMR2 register value
 * 
 * This function reads the 16 bits TMR2 register value and return it
 * 
 * @pre Initialize the TMR2 before calling this function
 */
unsigned int TMR2_ReadTime_us(void);

/**
 * @brief Writes the 16 bits value to TMR2 register
 * 
 * This function writes the 16 bits value to TMR2 register
 * This function must be called after the initialization of TMR2
 * 
 * @pre Initialize the TMR2 before calling this function
 * 
 * @param timerVal[unsigned short] - Value to write into TMR2 register
 */
void TMR2_WriteTimer(unsigned short timerVal);


/**
 * @brief Reload the 16 bits value to TMR2 register
 * 
 * This function reloads the 16 bit value to TMR2 register
 * This function must be called after the initialization of TMR2
 * 
 * @pre Initialize the TMR2 before calling this function
 */
void TMR2_Reload(void);

/**
 * @brief Timer Interrupt Service Routine
 * 
 * Timer Interrupt Service Routine is called by the Interrupt Manager
 * 
 * @pre Initialize the TMR2 module with interrupt before calling this isr
*/
void TMR2_ISR(void);


#endif /* TMR2_DRIVER_H */
