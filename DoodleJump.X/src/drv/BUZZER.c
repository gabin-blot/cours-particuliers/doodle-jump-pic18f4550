/**
 * \file 		BUZZER.c
 * \brief 		BUZZER driver source file
 * \authors 	Pierre Bergeras
 * \version 	1.0
 * \date 		15/11/2021 - 27/11/2021
 *
 * The BUZZER driver is responsible for controlling the 
 * the buzzer embedded on the EasyPIC v7 board.
 * 
 * It allows you to switch on or off these buzzer 
 * 
 *  
 * \pre       First initialize the driver with BUZZER_Init() function
 * 
 */

#include <xc.h>
#include "BUZZER.h"
#include "TMR2.h"
#include "PWM.h"

// Global variables
unsigned int    BuzzerPeriod = 0;

/**
 * @brief      Initializes the buzzer driver
 * 
 * Set RC2 as output
 * Init BUZZER at off state
 * 
 */
void BUZZER_Init(void)
{
    TRISCbits.RC2 = 0;
    BUZZER_Off();  
    TMR2_Init();
    PWM_Init();
}

/**
 * @brief      Turn on a specific BUZZER
 */
void BUZZER_On(void)
{
    TRISCbits.RC2 = 0;
}


/**
 * @brief      Turn off a specific BUZZER
 */
void BUZZER_Off(void)
{
    TRISCbits.RC2 = 1;
}
