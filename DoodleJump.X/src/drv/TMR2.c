/**
 * \file 		tmr2-driver.h
 * \brief 		Timer 2 driver source file
 * \authors 	Gabin Blot
 * \version 	1.0
 * \date 		15/08/2020
 *
 * Source code of the timer 2 driver
 * 
 *  
 * \pre       First initialize the driver with TMR2_Init() function
 */


/**
  Section: Included Files
*/

#include <xc.h>
#include "TMR2.h"

/**
  Section: TMR2 APIs
*/

void TMR2_Init(void)
{
    PR2 = 255; // as long as possible

    // TMR2 0; 
    TMR2 = 0x00;

    // Clearing IF flag.
    PIR1bits.TMR2IF = 0;

    // T2CKPS 1:16; T2OUTPS 1:16; TMR2ON on; 
    T2CON = 0x7F;
}