/**
 * \file 		EEPROM.c
 * \brief 		EEPROM driver source file
 * \authors 	Pierre Bergeras
 * \version 	1.0
 * \date 		15/11/2021 - 27/11/2021
 *
 * The EEPROM driver is responsible for controlling the 
 * the EEPROM embedded in the PIC18F4550
 * 
 * It allows you to read and write on EEPROM
 * 
 * \pre       None
 */

#include <xc.h>
#include <string.h>
#include "EEPROM.h"

/**
 * @brief   Write data to EEPROM
 * @param   data[unsigned char] - Data to write on EEPROM
 * @param   address[unsigned char] - Address where to write on EEPROM
 * 
 * Write data on EEPROM
 * 
 */
void EEPROM_write(unsigned char data, unsigned char address){
    EEADR = address;
    EEDATA = data;
    
    EECON1bits.EEPGD = 0;   // Access EEPROM not FLASH
    EECON1bits.CFGS = 0;    // Access memory not configuration
    EECON1bits.WREN = 1;    // Enable write access
    INTCONbits.GIE = 0;     // Disable interruption
    
    EECON2 = 0x55;          // Write protection header 1
    EECON2 = 0xAA;          // Write protection header 2
    
    EECON1bits.WR = 1;      // Start writing...
    
    INTCONbits.GIE = 1;     // Enable interrupions
    while(EECON1bits.WR);   // Wait for write flag disabled
    
    EECON1bits.WREN = 0;    // Disable write access
}

/**
 * @brief   Read data to EEPROM
 * @param   address[unsigned char] - Address where to read from
 * 
 * Read data on EEPROM
 * 
 */
char EEPROM_read(unsigned char address){
    EEADR = address;
    EECON1bits.EEPGD = 0;   // Access EEPROM not FLASH
    EECON1bits.CFGS = 0;    // Access memory not configuration
    
    EECON1bits.RD = 1;      // Start reading...    
    while(EECON1bits.RD);   // Wait for read flag disabled
    
    return(EEDATA);
}

/**
 * @brief   Write null ended string to EEPROM
 * @param   msg[char *] - String pointer to write on EEPROM
 * @param   address[unsigned char] - Address where to write on EEPROM
 * 
 * Write string on EEPROM
 * 
 */
void EEPROM_writeString(char* msg, unsigned char address){
    if(msg == NULL){
        return;
    }
    unsigned int size = strlen(msg);
    
    for(unsigned int i = 0 ; i < size ; i++){
        if(address+i > 255) { // prevent overflow
            return;
        }
        EEPROM_write(msg[i], address + (unsigned char) i);
    }
}

/**
 * @brief   Read null ended string to EEPROM
 * @param   msg[char *] - String to write with EEPROM data
 * @param   size[unsigned int] - Max size of the string
 * @param   address[unsigned char] - Address where to start reading
 * @ret     [unsigned int] - Number of bytes read and written to msg
 * 
 * Read string from EEPROM
 * 
 */
unsigned int EEPROM_readString(char * msg, unsigned int size, unsigned char address) {
    unsigned int read = 0;
    
    if(msg == NULL) {
        return 0;
    }
    
    for (read = 0 ; read < size ; read++){
        if(address+read > 255) { // prevent overflow
            msg[read] = '\0'; // force null termination
            return read;
        }
        
        msg[read] = EEPROM_read(address + (unsigned char) read);
        
        if(msg[read] == '\0')
        {
            return read;
        }
    }
    return read;
}

