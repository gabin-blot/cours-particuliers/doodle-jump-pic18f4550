
#ifndef TMR0_DRIVER_H
#define TMR0_DRIVER_H

#include <stdint.h>
#include <stdbool.h>


/**
  Section: Macro Declarations
*/

#define _INIT_TMR0  0xC2F6
#define _MAX_TMR0   0xFFFF 
#define _MIN_TMR0   _INIT_TMR0 

/**
  Section: TMR0 APIs
*/

/**
 * @brief Initializes the TMR0
 *  This function initializes the TMR0 Registers.
 *  This function must be called before any other TMR0 function is called.
 */
void TMR0_Init(void);


/**
 * @brief This function starts the TMR0
 * 
 * This function starts the TMR0 operation
 * This function must be called after the initialization of TMR0
 * 
 * @pre Initialize the TMR0 before calling this function
 */
void TMR0_StartTimer(void);


/**
 * @brief This function stops the TMR0
 * 
 * This function stops the TMR0 operation
 * This function must be called after the start of TMR0
 * 
 * @pre Initialize the TMR0 before calling this function
 */
void TMR0_StopTimer(void);


/**
 * @brief Reads the 16 bits TMR0 register value
 * 
 * This function reads the 16 bits TMR0 register value and return it
 * 
 * @pre Initialize the TMR0 before calling this function
 */
unsigned short TMR0_ReadTimer(void);

/**
 * @brief Writes the 16 bits value to TMR0 register
 * 
 * This function writes the 16 bits value to TMR0 register
 * This function must be called after the initialization of TMR0
 * 
 * @pre Initialize the TMR0 before calling this function
 * 
 * @param timerVal[unsigned short] - Value to write into TMR0 register
 */
void TMR0_WriteTimer(unsigned short timerVal);


/**
 * @brief Reload the 16 bits value to TMR0 register
 * 
 * This function reloads the 16 bit value to TMR0 register
 * This function must be called after the initialization of TMR0
 * 
 * @pre Initialize the TMR0 before calling this function
 */
void TMR0_Reload(void);

/**
 * @brief Timer Interrupt Service Routine
 * 
 * Timer Interrupt Service Routine is called by the Interrupt Manager
 * 
 * @pre Initialize the TMR0 module with interrupt before calling this isr
*/
void TMR0_ISR(void);


#endif /* TMR0_DRIVER_H */
