    /*
 * File:   usart.c
 * Author: Emilie
 *
 * Created on 25 novembre 2020, 21:56
 */

#include "USART.h"
void usartInit(int baudRate){
    
    TRISCbits.RC6 = 1; //TX pin
    TRISCbits.RC7 = 1; //RX pin
    //int n = (Fosc/(baudRate * 64)) - 1; //Formule pour calculer la valeur de SPBRGH:SPBRG 
    SPBRG = 12;
    TXSTA = 0x20; //transmit activ�, asynchrone, 8 bit, SYNC,BRGH = 0
    RCSTA = 0x90; //pin en TX - RX activ�, reception activ�
}

char usartCheck(){
    ///!\\\boucle � l'infini si on saisit pas de data///!\\\*
  while(!PIR1bits.RCIF && !(PORTE & 0b00000111) ); //On attend qu'on re�oit
    if(PIR1bits.RCIF){ //interruption flag de rd�c�ption
        return RCREG; //registre o� se trouve la data
    }
    else if(PORTE & 0b00000111)
    {
        switch(PORTE & 0b00000111){
            case 0b00000001:
                return 'd';
            case 0b00000010:
                return 's';
            case 0b00000100:
                return 'q';
        }
    }
    return 0;
}

char usartRead(){
    ///!\\\boucle � l'infini si on saisit pas de data///!\\\*
   //while(!PIR1bits.RCIF); //On attend qu'on re�oit
    
    if(PIR1bits.RCIF){ //interruption flag de rd�c�ption
        return RCREG; //registre o� se trouve la data
    }
    return 0;
}
char usartWrite(char c){
    if(PIR1bits.TXIF){ //interruption flag d'envoie
        TXREG = c; //registre qui transmet la data
        return 1;
    }
    return 0; //Si pas envoyer
}



