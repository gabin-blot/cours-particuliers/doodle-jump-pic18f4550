/**
 * \file 		PWM.h
 * \brief 		PWM driver header file
 * \authors 	Pierre Bergeras
 * \version 	1.0
 * \date 		15/11/2021 - 27/11/2021
 *
 * The PWM driver is responsible for controlling the 
 * the PWM embedded in the CCP1 module
 * 
 * It allows you to setup PWM module from CCP1
 * 
 * \pre       First initialize the driver with PWM_init() function
 */

#pragma once /** Guard against multiple inclusions */

#include <xc.h> /** include processor files - each processor file is guarded.  */

/** Prototype definitions */

void PWM_Init(void);
void PWM_On(void);
void PWM_Off(void);


