/**
 * \file 		BUZZER.h
 * \brief 		BUZZER driver header file
 * \authors 	Pierre Bergeras
 * \version 	1.0
 * \date 		15/11/2021 - 27/11/2021
 *
 * The BUZZER driver is responsible for controlling the 
 * the buzzer embedded on the EasyPIC v7 board.
 * 
 * It allows you to switch on or off these buzzer 
 * 
 *  
 * \pre       First initialize the driver with BUZZER_Init() function
 * 
 */

#pragma once

/** Prototype definitions */

void BUZZER_Init(void);
void BUZZER_On(void);
void BUZZER_Off(void);
