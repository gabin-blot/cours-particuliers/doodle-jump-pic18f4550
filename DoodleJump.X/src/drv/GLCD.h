/**
 * \file 		GLCD.h
 * \brief 		GLCD driver header file
 * \authors 	Pierre Bergeras
 * \version 	1.0
 * \date 		15/11/2021 - 27/11/2021
 *
 * The EEPROM driver is responsible for controlling the 
 * the display embedded on the EasyPIC v7 board.
 *  * 
 * \pre       First initialize the driver with G_Init() function
 */

#pragma once

#define 	GLCD_WIDTH          128
#define 	GLCD_WIDTH_CHIP     64
#define 	GLCD_HEIGHT         64
#define 	GLCD_PAGE_SIZE      8
#define 	GLCD_PAGE_NUMBER    GLCD_HEIGHT / GLCD_PAGE_SIZE

#define 	GLCD_CS1			LATBbits.LATB0
#define 	GLCD_CS2			LATBbits.LATB1
#define 	GLCD_RS				LATBbits.LATB2
#define 	GLCD_RW				LATBbits.LATB3
#define 	GLCD_E				LATBbits.LATB4
#define 	GLCD_RST			LATBbits.LATB5

#define 	GLCD_DATA_TRIS		TRISD
#define 	WR_DATA				LATD		
#define 	RD_DATA				PORTD	

#define 	GLCD_LEFT   	 	0
#define 	GLCD_RIGHT  	 	1	
	
#define 	GLCD_OFF			0
#define 	GLCD_ON				1

#define		OUT					0
#define		IN					1

#define		f3X6				0
#define		f8X8				1

#define XTAL_FREQ   8000000   // oscillator frequency for _delay()
#define _XTAL_FREQ  8000000


#include <htc.h>


//************************************************************************
// PROTOTYPES
//************************************************************************

void GLCD_Init(unsigned char mode);
void GLCD_WriteByte(unsigned char side, unsigned char data);
unsigned char GLCD_ReadByte(unsigned char side);
void GLCD_PlotPixel(unsigned char x, unsigned char y, unsigned char color);
void GLCD_SetCursor(unsigned char xpos,unsigned char ypos);
void GLCD_FillScreen(unsigned char color);
void GLCD_WriteChar( unsigned char ch, unsigned char color);
void GLCD_WriteString(unsigned char *str, unsigned char color);
void GLCD_Image(const unsigned char* img);
