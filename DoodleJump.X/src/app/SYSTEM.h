/**
 * \file 		SYSTEM.c
 * \brief 		System source file
 * \authors 	Pierre Bergeras
 * \version 	1.0
 * \date 		15/11/2021 - 27/11/2021
 *
 * Source code for system initialisation

 * 
 * \pre       First initialize the driver with SYSTEM_Init() function
 */

#pragma once

#define Fosc 8000000
#define _XTAL_FREQ 8000000

void SYSTEM_Init(void);
