#include "object.h"
#include <htc.h>
#include "../drv/BUZZER.h"

//M�thode permettant d'afficher notre Doodle
void displayDoodle(t_doodle * Doodle1)
{
    //Pour chaque Tableau de ArrayDoodle
	for(unsigned char i=0;i<12;i++)
    {
        //Pour chaque element du Tableau
        for(unsigned char j=0;j<12;j++)
        {
            //Si l'element en question vaut 1
            if(arrayDoodle[i][j]==1)
            {
                //Si on d�borde pas sur les x
                if(Doodle1->t_y+i<0x40)
                {
                    //Si on d�borde pas sur les y
                    if(Doodle1->t_x+j<0x80)
                    {
                        //On �crit un pixel au coord t_x+j et t_y+i
                         GLCD_PlotPixel(Doodle1->t_x+j,Doodle1->t_y+i,1);
                    } 
                }
            }   
        }
    }
}
//Methode Permettant d'afficher une plateforme
void displayObject(t_plat* obj)
{
	for(unsigned char i=0;i<4;i++)
    {
        for(unsigned char j=0;j<15;j++)
        {
            if(obj->type==PLATEFORME)
            {     
                if(arrayPlat[i][j]==1)
                {
                    if(obj->t_y+i<0x40)
                    {
                        if(obj->t_x+j<0x80)
                        {                           
                             GLCD_PlotPixel(obj->t_x+j,obj->t_y+i,"black");
                        } 
                    }
                }
            }
            if(obj->type==BROKEN)
            {
                if(arrayBrok[i][j]==1)
                {
                    if(obj->t_y+i<0x40)
                    {
                        if(obj->t_x+j<0x80)
                        {                           
                             GLCD_PlotPixel(obj->t_x+j,obj->t_y+i,"black");
                        } 
                    }
                }
            }
            if(obj->type==JUMP)
            {
                if(arrayJump[i][j]==1)
                {
                    if(obj->t_y+i<0x40)
                    {
                        if(obj->t_x+j<0x80)
                        {                           
                             GLCD_PlotPixel(obj->t_x+j,obj->t_y+i,"black");
                        } 
                    }
                }
            }
        }
    }
}

//Fonction permettant de tester si le doodle touche nne platforme
void testplat(t_game* myGame,t_doodle*Doodle1)
{
    unsigned char y =myGame->plat[1].t_y;
    //Pour tous les objets � l'ecran 
    for(int i=0;i<12;i++)
    {
        y =myGame->plat[i].t_y;
        //Si l'objet est activ�
        if(myGame->vecteur[i]==ACTIVE)
        {
            //Si les coordonn�es y sont bonnes
            if(myGame->Doodle1->t_y+10==y)
            {
                //Les pieds du doodle vont de x � x+11 et donc peuvent to
                if((myGame->plat[i].t_x)<=(myGame->Doodle1->t_x+11) && myGame->Doodle1->t_x+1<=(myGame->plat[i].t_x+15))
                {
                     //Si oui on mets le mouvement du doodle en Up
                    if(myGame->plat[i].type==PLATEFORME)
                    {
                        Doodle1->updown=UP;
                        //l'�nergie pour monter est initialiser � 4
                        Doodle1->watchdog=15;
                    }
                    else if(myGame->plat[i].type==JUMP)
                    {
                        Doodle1->updown=UP;
                        //l'�nergie pour monter est initialiser � 8
                        Doodle1->watchdog=30;
                      
                    }
                    else if(myGame->plat[i].type==BROKEN)
                    {
                        Doodle1->updown=DOWN;
                        //l'�nergie pour monter est initialiser � 0
                        Doodle1->watchdog=0;
                    }
                }      
            }
            //Si les coordonn�es ne sont pas bonnes  
        }  
    }
    
}
//Fonction permettant de faire bouger le Doodle
void displayScreen(t_game* myGame)
{
    GLCD_FillScreen(0);
    //Pour tous les objets
    for(int i=0;i<12;i++)
    {
        //On affiche nos plateforme   
        if(myGame->vecteur[i]==ACTIVE)
        {
            displayObject(&myGame->plat[i]);  
        } 
    }
    //On affiche notre Doodle
    displayDoodle(&myGame->Doodle1);
}
//Fonction qui sauvegarde la partie dans l'EEPROM
void saveEEPROM(t_game* myGame)
{
    //Permet de v�rifier si une partie est save
    EEPROM_write('s',0);
    //Les premi�res valeurs de l'EEPROM sont celles du Doodle
    EEPROM_write(myGame->Doodle1->t_x,1);
    EEPROM_write(myGame->Doodle1->t_y,2);
    EEPROM_write(myGame->Doodle1->updown,3);
    EEPROM_write(myGame->Doodle1->watchdog,4);
    
    char address=5;
    //On sauvegarde les plateformes
    for(int i=0;i<12;i++)
    {
        EEPROM_write(myGame->plat[i].t_x,address);
        address++;
        EEPROM_write(myGame->plat[i].t_y,address);
        address++;
        EEPROM_write(myGame->plat[i].type,address);
        address++;
        EEPROM_write(myGame->vecteur[i],address);
        address++;
    }
    
    EEPROM_write(myGame->Buffer->score,address);
    
}

//Fonction qui affiche l'�volution sur l'�cran
extern void updateDoodle(t_game* myGame)
{
    //On test Output
  //On verifie l'�tat du doodle DOWN
    while(myGame->Doodle1->updown==DOWN)
    {
        //S'il tombe on lui fait gagner un pixel
        myGame->Doodle1->t_y++;
        if(input==DROITE || (PORTE & 0b00000001 ))
        {
             myGame->Doodle1->t_x++;
        }
        else if(input==GAUCHE || (PORTE & 0b00000100 ))
        {
            myGame->Doodle1->t_x--;
        }
        else if(input==SAVE || (PORTE & 0b00000010 ))
        {
            saveEEPROM(myGame);
            return;
        }
        //On verifie si le doodle touche ou nom une plateforme
        testplat(myGame,&myGame->Doodle1);
        //On teste la condition de d�faite
        if(myGame->Doodle1->t_y==0x40)
        {
            GLCD_FillScreen(0);
            GLCD_SetCursor(28,3);
            GLCD_WriteString("GAME OVER", 1);
            BUZZER_On();  
            __delay_ms(100);
            BUZZER_Off();  
            __delay_ms(150);
            BUZZER_On();  
            __delay_ms(100);
            BUZZER_Off();  
            __delay_ms(150);
            BUZZER_On();  
            __delay_ms(100);
            BUZZER_Off();  
            __delay_ms(150);
            reset=RES;
          return;             
        }
         //On actualise la position
        displayScreen(myGame);
    }
    //Si notre DOODLE SAUTE
    while(myGame->Doodle1->updown==UP)
    {
        //Tant qu'il a de l'�nergie
        while(myGame->Doodle1->watchdog!=0)
        {
            if(input==DROITE || (PORTE & 0b00000001 ))
            {
                 myGame->Doodle1->t_x++;
            }
            else if(input==GAUCHE || (PORTE & 0b00000100 ))
            {
                myGame->Doodle1->t_x--;
            }
            else if(input==SAVE || (PORTE & 0b00000010 ))
            {
                saveEEPROM(myGame);
                return;
            }
            //Deux actions possibles
            //S'il est dans la partie basse de l'�cran
           if(myGame->Doodle1->t_y>=0x05)
            { 
              //Notre Doodle monte de 1 sur l'�cran 
              myGame->Doodle1->t_y-=1;
              //p_y-=1;
              //Notre Energie baisse de 1
              myGame->Doodle1->watchdog--;   
              //Notre Score augmente
              myGame->Buffer->score+=1;
              //L'environnement baisse de 1
              for(int i=0;i<12;i++)
              {
                  //Si l'objet est dans l'�cran
                  if(myGame->plat[i].t_y<0x40)
                  {                                 
                   myGame->plat[i].t_y+=1;                                   
                  }                                 
                   //Si il sort de l'�cran, on le remets en haut
                   else                                
                   {                               
                     unsigned char randtype = rand()/(RAND_MAX/20);
                    if(randtype!=BROKEN)
                    {
                        if(randtype>=14)
                        {
                            randtype=JUMP;
                        }
                        else
                        {
                            randtype=PLATEFORME;
                        }
                    }
                    unsigned char randx =rand()/(RAND_MAX/128);
                    myGame->plat[i].t_x=randx;
                    myGame->plat[i].t_y=0;
                    myGame->plat[i].type=randtype;                                  
                   }
              }
              //On actualise la position
              displayScreen(myGame);                                               
                                        
            }
           //S'il est dans la partie haute de l'�cran
           else
           {
               myGame->Doodle1->t_y+=1;
               for(int i=0;i<3;i++)
               {
                   if(myGame->Doodle1->watchdog>0)
                   {
                       myGame->Doodle1->watchdog--;
                   }
               }
               myGame->Buffer->score+=1;
                //L'environnement baisse de 3
              for(int i=0;i<12;i++)
              {
                  //Si l'objet est dans l'�cran
                  if(myGame->plat[i].t_y<0x40)
                  {                                 
                   myGame->plat[i].t_y+=3; 
                   
                  }                                 
                   //Si il sort de l'�cran, on le remets en haut
                   else                                
                   {                               
                    unsigned char randtype = rand()/(RAND_MAX/20);
                    if(randtype!=BROKEN)
                    {
                        if(randtype>=14)
                        {
                            randtype=JUMP;
                        }
                        else
                        {
                            randtype=PLATEFORME;
                        }
                    }
                    unsigned char randx =rand()/(RAND_MAX/128);
                    myGame->plat[i].t_x=randx;
                    myGame->plat[i].t_y=0;
                    myGame->plat[i].type=randtype;                                 
                   }                                                                
              }
              //On actualise la position
              displayScreen(myGame); 
           }
        }
        //Le watchdog est � pr�sent � 0
        myGame->Doodle1->updown=DOWN;
        
        //Ecriture des donn�es dans l'eeprom    
        
    } 
} 
void __interrupt() isr(void) {
    if (PIR1bits.RCIF == 1) 
    {                                     // if timer flag is set & interrupt enabled
        input = usartRead();
    }
}

void initObject(t_game* myGame)
{
    GLCD_FillScreen(0);//On clear le screen
    //On initialise notre vairable globale Doodle
    myGame->Doodle1->t_x=0x00+64;
    myGame->Doodle1->t_y=0x25;
    myGame->Doodle1->updown=DOWN; 
    myGame->Doodle1->watchdog=0;
    
    input=0;
    
    //Initialisation des plateformes
    //La plateforme de base doit �tre accessible
    myGame->plat[0].t_x=0x00+64;
    myGame->plat[0].t_y=0x38;
    myGame->plat[0].type=PLATEFORME;
    myGame->vecteur[0]=ACTIVE;
    
    for(int i=1;i<12;i++)
    {
        int compteur=0;
        unsigned char randx =rand()/(RAND_MAX/2);
        if(randx==0)
        {
            myGame->plat[i].t_x=myGame->plat[i-1].t_x-(i*5);
        }
        else if(randx==1)
        {
            myGame->plat[i].t_x=myGame->plat[i-1].t_x+10;
        }
        //On veut baisser les chances que ce soit BROKEN
        unsigned char randtype = rand()/(RAND_MAX/20);
        if(randtype!=BROKEN)
        {
            if(randtype>=14)
            {
                randtype=JUMP;
            }
            else
            {
                randtype=PLATEFORME;
            }
        }
        myGame->plat[i].t_y=myGame->plat[i-1].t_y-5;
        myGame->plat[i].type=randtype;
        myGame->vecteur[i]=ACTIVE;
    }   
    myGame->Buffer->t_code=land;
    reset=SET;
    GLCD_FillScreen(0);
    displayScreen(myGame);
}

void initEEPROM(t_game* myGame)
{
    GLCD_FillScreen(0);//Clear screen
    //On initialise notre variable globale Doodle
    myGame->Doodle1->t_x=EEPROM_read(1);
    myGame->Doodle1->t_y=EEPROM_read(2);
    myGame->Doodle1->updown=EEPROM_read(3); 
    myGame->Doodle1->watchdog=EEPROM_read(4);
    
    char adresse=5;
    input=0;
    //Valeur des pieds du Doodle
    
    //Initialisation des plateformes
    //La plateforme de base doit �tre accessible
    
    
    for(int i=0;i<12;i++)
    {
        myGame->plat[i].t_x=EEPROM_read(adresse);
        adresse++;
        myGame->plat[i].t_y=EEPROM_read(adresse);
        adresse++;
        myGame->plat[i].type=EEPROM_read(adresse);
        adresse++;
        myGame->vecteur[i]=EEPROM_read(adresse);
        adresse++;
    }   
    myGame->Buffer->score=EEPROM_read(adresse);
    myGame->Buffer->t_code=land;
    reset=SET;
    GLCD_Image(land);
    GLCD_FillScreen(0);
    displayScreen(myGame);
}

void debut(t_game* myGame)
{
    char test='0';
    GLCD_Image(terrain1);
    
    while(test=='0')
    {
        INTCONbits.GIE = 0; 
        test=usartCheck();
        INTCONbits.GIE = 1; 
        if(test=='q')
        {
            initObject(myGame);
            return;
        }
        else if(test=='d') 
        {
            initEEPROM(myGame);
            return;
        }
    }
}