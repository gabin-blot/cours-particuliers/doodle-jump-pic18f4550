#ifndef OBJETS_H
#define	OBJETS_H

#include <stdio.h>
#include <stdlib.h>
#include "bitmap.h"
#include <xc.h>
#include "../drv/EEPROM.h"
#include "../drv/USART.h"
#include "../drv/GLCD.h"

#define 	BROKEN              0
#define 	PLATEFORME			1
#define 	JUMP                2

#define     DOWN                0
#define     UP                  1

#define     DESACTIVE           0
#define     ACTIVE              1

#define     RES                 0
#define     SET                 1

#define     GAUCHE              'q'
#define     DROITE              'd'
#define     SAVE                's'
//Construction des objets

//Construction des buffers � l'�cran
typedef struct plateforme{
    unsigned char t_x,t_y;
    unsigned char type;
}t_plat;

typedef struct ecran{
    unsigned char* t_code;
    unsigned char score;
}t_ecran;
//Construction de mon objet doodle.
typedef struct doodle
{
	unsigned char t_x,t_y;
    unsigned char updown;
    unsigned char watchdog;
}t_doodle;

typedef struct game
{
    t_ecran Buffer[1];
    t_plat plat[12];
    t_doodle Doodle1[1];
    unsigned char vecteur[12];
}t_game;
//Variable Globale
unsigned char  input;
unsigned int  reset;

extern const unsigned char Font8x8[2048];
//Prototype des fonctions
//Fonction d'affichage du Doodle
extern void displayDoodle(t_doodle * Doodle1);
//Fonction modifiant l'affichage
extern void updateDoodle(t_game* myGame);
//Fonction affichant une plateforme
extern void displayObject(t_plat* obj);
//Fonction qui affiche l'�cran
extern void displayScreen(t_game* myGame);
extern void initObject(t_game* myGame);
extern void initEEPROM(t_game* myGame);
extern void debut(t_game* myGame);
#endif	/* OBJETS_H */

