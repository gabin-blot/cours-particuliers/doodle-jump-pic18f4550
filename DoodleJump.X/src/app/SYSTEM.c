/**
 * \file 		SYSTEM.c
 * \brief 		System source file
 * \authors 	Pierre Bergeras
 * \version 	1.0
 * \date 		15/11/2021 - 27/11/2021
 *
 * Source code for system initialisation

 * 
 * \pre       First initialize the driver with SYSTEM_Init() function
 */

#include <xc.h>
#include "SYSTEM.h"

// --- init the PIC18F device
void SYSTEM_Init(void)
{
	// PORTA digital
	ADCON1 = 0x0F ;
	ADCON0 = 0;

	// set all ports as OUTPUTS
	TRISA = 0x00;
	TRISB = 0x00;
	TRISC = 0x00;
	TRISD = 0x00;
	TRISE = 0x07;

	// set port by port on "all zeros"
	PORTA = 0x00;
	PORTB = 0x00;
	PORTC = 0x00;
	PORTD = 0x00;
    // make sure to have an empty LAST line in any *.c file (just hit an Enter)!

	PORTE = 0x00;
    
    // Set Interrupt Enable bits
    PIE1bits.RCIE = 1;     

    //Set global Interrupt Enable bits
    INTCONbits.GIE = 1;     // global interrput enable
    INTCONbits.PEIE = 1;    // Peripheral Interrupt Enable bit
       
}
